# daverona/node

This is a Node.js module template.

## Quick Start

## Development

## Production

Please read [https://gitlab.com/daverona/docs/javascript/-/blob/master/repo.md](https://gitlab.com/daverona/docs/javascript/-/blob/master/repo.md).

## References

* Creating Node.js modules: [https://docs.npmjs.com/creating-node-js-modules](https://docs.npmjs.com/creating-node-js-modules)
